<?php

declare(strict_types=1);

namespace App\Task1;

use Exception;
use App\Functions\MyFunctions;

class Track
{
    private float $lapLength;
    private int $lapsNumber;
    private array $cars;

    public function __construct(float $lapLength, int $lapsNumber)
    {
        if ($lapLength <= 0) {
            throw new Exception('negative lap length');
        }
        if ($lapsNumber <= 0) {
            throw new Exception('negative laps number');
        }
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
       $this->cars[] = $car;
    }

    public function all(): array
    {
        if (!isset($this->cars)) {
            throw new Exception('Typed property App\Task1\Track::$cars must not be accessed before initialization');
        }
        return $this->cars;
    }

    private function runCar(Car $car): float
    {
        $raceLength = $this->getLapLength()*$this->getLapsNumber();
        $toRefueling = $car->getFuelTankVolume()/$car->getFuelConsumption()*100;
        $lapWithoutRefueling = floor($raceLength/$toRefueling);
        if ($lapWithoutRefueling >= 0) {
            $trackTime = ($lapWithoutRefueling*$car->getPitStopTime())+($raceLength/$car->getSpeed()*60*60);
        }
        else {
            $trackTime = $raceLength/$car->getSpeed();
        }
        return $trackTime;
    }

    public function run(): Car
    {
        if (!isset($this->cars)) {
            throw new Exception('Typed property App\Task1\Track::$cars must not be accessed before initialization');
        }
        foreach ($this->cars as $key=>$car) {
           $car->trackTime = $this->runCar($car);
        }
        $this->cars = MyFunctions::sortObjectSetBy($this->cars, "trackTime");
        return $this->cars[0];
    }
}
