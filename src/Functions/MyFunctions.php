<?php

declare(strict_types=1);

namespace App\Functions;

class MyFunctions
{
    static public function sortObjectSetBy($objectSetForSort, $sortBy)
    {
        usort($objectSetForSort, function ($object1, $object2) use ($sortBy) {
                if ($object1->$sortBy == $object2->$sortBy) {
                    return 0;
                } elseif ($object1->$sortBy > $object2->$sortBy) {
                    return 1;
                } else {
                    return -1;
                }
            }
        );
        return $objectSetForSort;
    }
}
