<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Task1\Track;
use App\Task3\CarTrackHtmlPresenter;
use \App\Task1\Car;

$arena = new Track(4, 40);

$car1 = new Car(1,
    'https://pbs.twimg.com/profile_images/595409436585361408/aFJGRaO6_400x400.jpg',
    'BMW',
    250,
    10,
    5,
    15
);
$car2 = new Car(2,
    'https://i.pinimg.com/originals/e4/15/83/e41583f55444b931f4ba2f0f8bce1970.jpg',
    'Tesla',
    200,
    5,
    5.3,
    18
);
$car3 = new Car(3,
    'https://fordsalomao.com.br/wp-content/uploads/2019/02/1499441577430-1-1024x542-256x256.jpg',
    'Ford',
    220,
    5,
    6.1,
    18.5
);

$arena->add($car1);
$arena->add($car2);
$arena->add($car3);

$presenter = new CarTrackHtmlPresenter();
$presentation = $presenter->present($arena);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Built-in Web Server</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/fresh-bootstrap-table.css" rel="stylesheet" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="wrapper">

    <div class="fresh-table full-color-blue full-screen-table">
        <table id="fresh-table" class="table">
            <thead>
            <th data-field="id" data-sortable="true">Car ID</th>
            <th data-field="photo">Photo</th>
            <th data-field="name" data-sortable="true">Mark</th>
            <th data-field="speed" data-sortable="true">Speed</th>
            <th data-field="fuel-tank-volume" data-sortable="true">Fuel Tank Volume</th>
            <th data-field="fuel-consumption" data-sortable="true">Fuel Consumption</th>
            <th data-field="pit-stop-time" data-sortable="true">Pit-Stop Time</th>
            <th data-field="place" data-sortable="true">Place</th>
            <th data-field="time-on-track" data-sortable="true">Time on track</th>
            </thead>
            <tbody>
                <?php echo $presentation; ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-table.js"></script>
<script type="text/javascript">
        var $table = $('#fresh-table'),
            full_screen = false,
            window_height;

        $().ready(function(){

            window_height = $(window).height();
            table_height = window_height - 20;

            $table.bootstrapTable({
                toolbar: ".toolbar",

                showRefresh: false,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                striped: true,
                sortable: false,
                height: table_height,
                pageSize: 25,
                pageList: [25,50,100],

                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });

            $(window).resize(function () {
                $table.bootstrapTable('resetView');
            });
        });
    </script>
</body>
</html>