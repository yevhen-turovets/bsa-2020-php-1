<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $html = (string)'';
        $place = (int)0;
        $track->run();
        $cars = $track->all();
        foreach ($cars as $car) {
            $html .= '<tr title="'.$car->getName().': '.$car->getSpeed().', '.$car->getFuelTankVolume().'">';
            $html .= '<td>'.$car->getId().'</td>';
            $html .= '<td><img src="'.$car->getImage().'"></td>';
            $html .= '<td>'.$car->getName().'</td>';
            $html .= '<td>'.$car->getSpeed().'</td>';
            $html .= '<td>'.$car->getFuelTankVolume().'</td>';
            $html .= '<td>'.$car->getFuelConsumption().'</td>';
            $html .= '<td>'.$car->getPitStopTime().'</td>';
            $html .= '<td>'.++$place.'</td>';
            $html .= '<td>'.date("H:i:s", (int)$car->trackTime).'</td>';
            $html .= '</tr>';
        }
        return $html;
    }
}
