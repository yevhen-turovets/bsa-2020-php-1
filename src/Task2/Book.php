<?php

declare(strict_types=1);

namespace App\Task2;

use Exception;

class Book
{
    private string $title;
    private int $price;
    private int $pagesNumber;

    public function __construct(string $title, int $price, int $pagesNumber)
    {
        if ($price <= 0) {
            throw new Exception("negative price");
        }
        if ($pagesNumber <= 0) {
            throw new Exception("negative number of pages");
        }
        $this->title = $title;
        $this->price = $price;
        $this->pagesNumber = $pagesNumber;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }
}
