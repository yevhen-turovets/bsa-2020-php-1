<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    private int $minPagesNumber;
    private array $libraryBooks;
    private int $maxPrice;
    private array $storeBooks;

    public function __construct(
        int $minPagesNumber,
        array $libraryBooks,
        int $maxPrice,
        array $storeBooks
    ) {
        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks = $libraryBooks;
        $this->maxPrice = $maxPrice;
        $this->storeBooks = $storeBooks;
    }

    public function generate(): \Generator
    {
        return $this->filteredBooks($this->libraryBooks,$this->storeBooks);
    }

    private function filteredBooks ($libraryBooks,$storeBooks) {
        foreach ($libraryBooks as $value) {
            if ($value->getPagesNumber() >= $this->minPagesNumber) {
            yield $value;
            }
        }
        foreach ($storeBooks as $value) {
            if ($value->getPrice() < $this->maxPrice) {
                yield $value;
            }
        }
    }
}
